FROM node:8

# ENV PATH=/usr/bin:${PATH}
# RUN echo ${PATH}
# RUN echo $PATH

# RUN apk add --no-cache ffmpeg
RUN apt-get update && apt-get install -y ffmpeg
# RUN which ffmpeg
# RUN ffmpeg
ENV PATH="${PATH}:/usr/local/bin"
RUN mkdir /app
WORKDIR /app

COPY package*.json /app/

RUN npm install


COPY . /app

EXPOSE 3333

CMD npm start