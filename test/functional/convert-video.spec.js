'use strict'

const { test, trait } = use('Test/Suite')('Convert Video')
const Helpers = use('Helpers')
const Drive = use('Drive')

trait('Test/Browser')

test('convert mp4 to avi', async({ browser, assert }) => {
    const page = await browser.visit('/')

    await page
        .attach('[name="video"]', [Helpers.tmpPath('testing/yooo.mp4')])
        .select('[name="type"]', 'video')
        .select('[name="format"]', 'avi')
        .select('[name="ratio"]', '2:1')
        .submitForm('form')

    const isMoved = await Drive.exists('videos/yooo.mp4')
    const isConverted = await Drive.exists('videos/yooo_c.avi')

    assert.isTrue(isMoved)
    assert.isTrue(isConverted)

})

test('convert avi to flv', async({ browser, assert }) => {
    const page = await browser.visit('/')

    await page
        .attach('[name="video"]', [Helpers.tmpPath('testing/yooo.avi')])
        .select('[name="type"]', 'video')
        .select('[name="format"]', 'flv')
        .select('[name="ratio"]', '3:1')
        .submitForm('form')


    const isMoved = await Drive.exists('videos/yooo.avi')
    const isConverted = await Drive.exists('videos/yooo_c.flv')


    assert.isTrue(isMoved)
    assert.isTrue(isConverted)

})

test('convert flv to mp4', async({ browser, assert }) => {
    const page = await browser.visit('/')

    await page
        .attach('[name="video"]', [Helpers.tmpPath('testing/yooo.flv')])
        .select('[name="type"]', 'video')
        .select('[name="format"]', 'mp4')
        .select('[name="ratio"]', '4:1')
        .submitForm('form')


    const isMoved = await Drive.exists('videos/yooo.flv')
    const isConverted = await Drive.exists('videos/yooo_c.mp4')


    assert.isTrue(isMoved)
    assert.isTrue(isConverted)

})