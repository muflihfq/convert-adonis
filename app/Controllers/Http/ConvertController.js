'use strict'

const Helpers = use('Helpers')

class ConvertController {
    async convert({ request, response }) {

        const type = request.post().type
        const format = request.post().format
        const ratio = request.post().ratio


        const file = request.file('video')
        const clientName = file.clientName
        const dotIndex = clientName.lastIndexOf('.')
        const fileName = clientName.substring(0, dotIndex) + '_c'

        await file.move(Helpers.tmpPath('videos'), {
            name: file.clientName,
            overwrite: true
        })

        if (!file.moved()) {
            response.status(400).send('error')
        }


        const isFormatted = await this.format(clientName,
            fileName, format, ratio)

        if (isFormatted) {
            response.attachment(
                'tmp/videos/' + fileName + '.' + format)
        } else {
            response.status(400).send('error')
        }


    }

    format(clientName, fileName, format, ratio) {
        return new Promise(resolve => {

            // const ffmpegInstaller = require('@ffmpeg-installer/ffmpeg').path;
            var ffmpeg = require('fluent-ffmpeg');

            // ffmpeg.setFfmpegPath(ffmpegInstaller.path)
            ffmpeg('tmp/videos/' + clientName)
                .format(format)
                .size(ratio)
                .on('error', function(err) {
                    console.log(err)
                    resolve(false)
                })
                .on('end', function() {
                    resolve(true)
                })
                .save('tmp/videos/' + fileName + '.' + format);
        })
    }
}

module.exports = ConvertController